#    This file is part of x57bit
#
#    x57bit is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    x57bit is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with x57bit.  If not, see <http://www.gnu.org/licenses/>.
#
#      Authors: Dirk Dittmann
#      Date: 17.05.2017
#
#      Last change:      Dirk Dittmann
#      Date:             17.05.2017
#


var bfpm = bitField.BitFieldPropertyMap.new();

bfpm.add("/controls/lighting/taxi-light",1,"BOOL");
bfpm.add("/controls/lighting/landing-lights",1,"BOOL");
bfpm.add("/controls/lighting/nav-lights",1,"BOOL");
bfpm.add("/controls/lighting/beacon",1,"BOOL");
bfpm.add("/controls/anti-ice/wiper",3,"INT");
bfpm.add("/controls/lighting/strobe",1,"BOOL");
bfpm.add("/controls/lighting/logo-lights",1,"BOOL");
bfpm.add("/controls/lighting/turn-off-lights",1,"BOOL");

bfpm.createBuffer();

var bfpm2 = bitField.BitFieldPropertyMap.new();

bfpm2.add("/controls/lighting/taxi-light2",1,"BOOL");
bfpm2.add("/controls/lighting/landing-lights2",1,"BOOL");
bfpm2.add("/controls/lighting/nav-lights2",1,"BOOL");
bfpm2.add("/controls/lighting/beacon2",1,"BOOL");
bfpm2.add("/controls/anti-ice/wiper2",3,"INT");
bfpm2.add("/controls/lighting/strobe2",1,"BOOL");
bfpm2.add("/controls/lighting/logo-lights2",1,"BOOL");
bfpm2.add("/controls/lighting/turn-off-lights2",1,"BOOL");


var update5Hz = func(){
    
    print("--- update Buffer");
    bfpm.updateBuffer();

    var intBuf0 = bfpm.toUnsignedInteger(0);
    var intBuf1 = bfpm.toUnsignedInteger(1);
    var strBuf = bfpm.toString();

    printf("strBuf 1 : %2X %2X %2X %2X",strBuf[0],strBuf[1],strBuf[2],strBuf[3]);
    printf("intBuf 1 : %X %X",intBuf0,intBuf1);

    setprop("/sim/multiplay/generic/string[0]", strBuf);
    setprop("/sim/multiplay/generic/int[0]", intBuf0);
    setprop("/sim/multiplay/generic/int[1]", intBuf1);
    
    print("--- Network transfare int");
    
    var value = getprop("sim/multiplay/generic/int[0]");
    
    bfpm2.setInteger(value);
    
    intBuf0 = bfpm2.toUnsignedInteger(0);
    intBuf1 = bfpm2.toUnsignedInteger(1);
    strBuf = bfpm2.toString();
    printf("strBuf 2 : %2X %2X %2X %2X",strBuf[0],strBuf[1],strBuf[2],strBuf[3]);
    printf("intBuf 2 : %X %X",intBuf0,intBuf1);

    print("--- update properties");
    bfpm2.updateProperty();
    
    
};

var timer5Hz = maketimer(0.2, func(){
    update5Hz();
});

timer5Hz.start();

#var listener = setlistener("")




