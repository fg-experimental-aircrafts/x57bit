#    This file is part of x57bit
#
#    x57bit is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    x57bit is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with x57bit.  If not, see <http://www.gnu.org/licenses/>.
#
#      Authors: Dirk Dittmann
#      Date: 17.05.2017
#
#      Last change:      Dirk Dittmann
#      Date:             17.05.2017
#

var testOperators = func(){
    var a = 0x05;
    var b = 0x03;
    var c = 0;
    
    
    c = a | b;
    printf("Bit  OR : %X | %X == %X" ,a,b,c);
    
    c = a ^ b;
    printf("Bit XOR : %X ^ %X == %X" ,a,b,c);
    
    c = a & b;
    printf("Bit AND : %X & %X == %X" ,a,b,c);
    
    c = ! a;
    printf("Bit NOT : ! %X == %X" ,a,c);
    
    c = a ;
    c |= b;
    printf("Bit  OR assign : %X |= %X == %X" ,a,b,c);
    
    c = a ;
    c ^= b;
    printf("Bit XOR assign : %X ^= %X == %X" ,a,b,c);
    
    c = a ;
    c &= b;
    printf("Bit AND assign : %X &= %X == %X" ,a,b,c);
    
    
}

var testDouble = func(){
    setprop("/controls/flight/spoilers",0.5);
    
    var value = getprop("/controls/flight/spoilers");
    
    var _buf = bits.buf(8);
    
    bits.setsfld(_buf, 0, 64, value);
    
    var intBuf0 = bits.fld(_buf, 0, 32);
    var intBuf1 = bits.fld(_buf, 32, 32);
    
    
    printf("intBuf double : %X %X",intBuf0,intBuf1);
    
    
}