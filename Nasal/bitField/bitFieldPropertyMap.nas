#    This file is part of x57bit
#
#    x57bit is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    x57bit is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with x57bit.  If not, see <http://www.gnu.org/licenses/>.
#
#      Authors: Dirk Dittmann
#      Date: 17.05.2017
#
#      Last change:      Dirk Dittmann
#      Date:             17.05.2017
#

var BitFieldProperty = {
    new: func(_path,_startbit,_length,_type = "INT"){
            var m = { parents: [BitFieldProperty] };
            m._path     = _path;
            m._startbit = _startbit;
            m._length   = _length;
            m._type     = _type; # "INT"|"DOUBLE"|"BOOL"|"STRING"
            
            return m;
    },
    
};

var BitFieldPropertyMap = {
    new: func(){
            var m = { parents: [BitFieldPropertyMap] };
            m._propertyIndex = [];
            m._propertyMap = {};
            m._count = 0;
            m._buf = nil;
            return m;
    },
    add: func(_path,_length,_type="INT"){
        var bfp = BitFieldProperty.new(_path,me._count,_length);
        me._propertyMap[_path] = bfp;
        append(me._propertyIndex,_path);
        me._count += _length;
    },
    remove: func(_path){
        ### TODO 
        # remove from _propertyMap & index
        # update buffer !
    },
    createBuffer: func(){
        var length = math.ceil(me._count / 32) * 4;
        printf("BitFieldPropertyMap::createBuffer() ... length %i",length);
        me._buf = bits.buf(length);
    },
    updateBuffer: func(){
        if (me._buf != nil){ 
            foreach(var _path;me._propertyIndex) {
                var bfp = me._propertyMap[_path];
                var value = getprop(bfp._path);
                bits.setfld(me._buf, bfp._startbit, bfp._length, value);
            }
        }
    },
    updateProperty: func(){
        if (me._buf != nil){ 
            foreach(var _path;me._propertyIndex) {
                var bfp = me._propertyMap[_path];
                var value = bits.fld(me._buf, bfp._startbit, bfp._length);
                setprop(bfp._path,value);
            }
        }
    },
    
    getPropUnsignedInteger: func(_path){
        var value = 0;
        if (contains(me._propertyMap,_path)){
            var bfp = me._propertyMap[_path];
            if (me._buf != nil){
                value = bits.fld(me._buf, bfp._startbit, bfp._length);
            }
        }
        return value;
    },
    getPropSignedInteger: func(_path){
        var value = 0;
        if (contains(me._propertyMap,_path)){
            var bfp = me._propertyMap[_path];
            if (me._buf != nil){
                value = bits.sfld(me._buf, bfp._startbit, bfp._length);
            }
        }
        return value;
    },
    toString: func(){
        if (me._buf != nil){
            return me._buf;
        }else{
            return "";
        }   
    },
    toUnsignedInteger: func(index=0){
        var value = 0;
        if (me._buf != nil){
            if (size(me._buf) >= (index+1) * 4){
                var startbit = index * 32;
                value = bits.fld(me._buf, startbit, 32);
            }
        }
        return value;
    },
    toSignedInteger: func(index=0){
        var value = 0;
        if (me._buf != nil){
            if (size(me._buf) >= (index+1) * 4){
                var startbit = index * 32;
                value = bits.sfld(me._buf, startbit, 32);
            }
        }
        return value;
    },
    setBuf : func(buf){
        #printf("BitFieldPropertyMap.setBuf() ... size %i",size(buf));
        me._buf = buf;
    },
    setInteger : func(value){
        me._buf = bits.buf(4);
        bits.setfld(me._buf, 0, 32, value);
        
        # evel hack : to rotate the bits again 
        value = bits.fld(me._buf, 0, 32);
        bits.setfld(me._buf, 0, 32, value);
    },
};